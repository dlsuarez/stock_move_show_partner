# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
import logging

class stock_move(models.Model):

    _inherit = 'stock.move'

    _logger = logging.getLogger(__name__)

    related_partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Partner',
        store=True,
        related="picking_id.partner_id",
    )
